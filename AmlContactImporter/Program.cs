﻿using System.Data.SqlClient;
using AmlContactImporter.Models.Features.DTO;
using Dapper;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;

namespace AmlContactImporter;


public class Program
{
    public const string EXCEL_FILEPATH = "C:\\temp\\Importazione_anagrafiche.xlsx";
    //public const string EXCEL_PATH = "C:\\Users\\ST394\\Desktop\\tmp";

    static async Task Main(string[] args)
    {
        try
        {
            IConfiguration config = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .AddEnvironmentVariables()
            .Build();

            string? coprelWebConnectionString = config.GetValue<string>("ConnectionStrings:AmlCrm");
            if (string.IsNullOrEmpty(coprelWebConnectionString)) await Log("ConnectionString AmlCrm mancante!");

            using var coprelConnection = new SqlConnection(coprelWebConnectionString);
            await coprelConnection.OpenAsync();

            await Log("Aperta connessione database Coprel");
            if (File.Exists(EXCEL_FILEPATH))
            {
                FileInfo existingFile = new FileInfo(EXCEL_FILEPATH);
                await Log($"Apro: {existingFile.Name}");

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using ExcelPackage package = new ExcelPackage(existingFile);
                if (package.Workbook == null || package.Workbook.Worksheets == null || package.Workbook.Worksheets.Count == 0)
                {
                    await Log($"Il file {existingFile.Name} è corrotto");
                    return;
                }

                ExcelWorksheet? worksheet = package.Workbook.Worksheets.FirstOrDefault();

                if (worksheet != null)
                {
                    int rowCount = worksheet.Dimension.End.Row;

                    var sql = "SELECT * FROM CustomerType";
                    var customerTypes = await coprelConnection.QueryAsync<CustomerTypeDTO>(sql);

                    for (int row = 3; row <= rowCount; row++)
                    {
                        var firstName = worksheet.Cells[row, 1].GetCellValue<string>();
                        var lastName = worksheet.Cells[row, 2].GetCellValue<string>();
                        var fiscalCode = worksheet.Cells[row, 3].GetCellValue<string>();
                        var vat = worksheet.Cells[row, 4].GetCellValue<string>();
                        var phoneNumber = worksheet.Cells[row, 5].GetCellValue<string>();
                        var mobileNumber = worksheet.Cells[row, 6].GetCellValue<string>();
                        var email = worksheet.Cells[row, 7].GetCellValue<string>();
                        var pec = worksheet.Cells[row, 8].GetCellValue<string>();
                        var sdi = worksheet.Cells[row, 9].GetCellValue<string>();
                        var customerType = worksheet.Cells[row, 10].GetCellValue<string>();

                        var customerTypeDTO = (from elem in customerTypes.Where(e => !string.IsNullOrEmpty(e.Name)
                                            && e.Name.Equals(customerType, StringComparison.InvariantCultureIgnoreCase))
                                               select elem).FirstOrDefault();

                        if (customerTypeDTO == null)
                        {
                            await Log($"Tipologia assente in riga {row}");
                            continue;
                        }

                        // Birth informations
                        var birthDate = worksheet.Cells[row, 11].GetCellValue<string>();
                        var birthCity = worksheet.Cells[row, 12].GetCellValue<string>();
                        var birthProvince = worksheet.Cells[row, 13].GetCellValue<string>();
                        var birthZipCode = worksheet.Cells[row, 14].GetCellValue<string>();
                        var birthCountry = worksheet.Cells[row, 15].GetCellValue<string>();

                        // Residence informations
                        var city = worksheet.Cells[row, 16].GetCellValue<string>();
                        var province = worksheet.Cells[row, 17].GetCellValue<string>();
                        var zipCode = worksheet.Cells[row, 18].GetCellValue<string>();
                        var country = worksheet.Cells[row, 19].GetCellValue<string>();
                        var address = worksheet.Cells[row, 20].GetCellValue<string>();

                        // Company informations
                        var isCompany = worksheet.Cells[row, 21].GetCellValue<string>();
                        //TODO: var inQualityOF = worksheet.Cells[row, 22].GetCellValue<string>();
                        var companyName = worksheet.Cells[row, 23].GetCellValue<string>();
                        var companyVat = worksheet.Cells[row, 24].GetCellValue<string>();
                        var companyFiscalCode = worksheet.Cells[row, 25].GetCellValue<string>();

                        var companyCity = worksheet.Cells[row, 26].GetCellValue<string>();
                        var companyProvince = worksheet.Cells[row, 27].GetCellValue<string>();
                        var companyZipCode = worksheet.Cells[row, 28].GetCellValue<string>();
                        var companyCountry = worksheet.Cells[row, 29].GetCellValue<string>();
                        var companyAddress = worksheet.Cells[row, 30].GetCellValue<string>();

                        var importedCustomer = new CustomerDTO
                        {
                            CustomerTypeId = customerTypeDTO.Id,
                            FirstName = firstName,
                            LastName = lastName,
                            FiscalCode = fiscalCode,
                            VAT = vat,
                            PhoneNumber = phoneNumber,
                            Email = email,
                            PEC = pec,
                            SDI = sdi,
                            BirthDate = birthDate,
                            BirthCity = birthCity,
                            BirthProvince = birthProvince,
                            BirthZipCode = birthZipCode,
                            BirthCountry = birthCountry,
                            City = city,
                            Province = province,
                            ZipCode = zipCode,
                            Country = country,
                            Address = address,
                            IsCompany = !string.IsNullOrEmpty(isCompany) && isCompany.Equals("Si", StringComparison.InvariantCultureIgnoreCase) ? true : false,
                            CompanyCity = companyCity,
                            CompanyProvince = companyProvince,
                            CompanyZipCode = companyZipCode,
                            CompanyCountry = companyCountry,
                            CompanyAddress = companyAddress,
                            InsertedBy = "IMPORT",
                            InsertedDate = DateTime.Now,
                            ModifiedBy = "IMPORT",
                            ModifiedDate = DateTime.Now,
                        };

                        await Log(importedCustomer.ToString());

                        // TODO: folder
                        string query = @"INSERT INTO Customer(CustomerTypeId, FirstName, LastName, CompanyName, VAT, FiscalCode, Email, PEC, SDI, 
                            PhoneNumber, Country, City, Address, ZipCode, Province, BirthCountry, BirthCity, BirthZipCode, BirthProvince, 
                            BirthDate, IsCompany, CompanyCountry, CompanyCity, CompanyAddress, CompanyZipCode, CompanyProvince,
                            Note, ModifiedBy, InsertedBy, ModifiedDate, InsertedDate)
                            VALUES(@CustomerTypeId, @FirstName, @LastName, @CompanyName, @VAT, @FiscalCode, @Email, @PEC, @SDI,
                            @PhoneNumber, @Country, @City, @Address, @ZipCode, @Province, @BirthCountry, @BirthCity, @BirthZipCode, @BirthProvince,
                            @BirthDate, @IsCompany, @CompanyCountry, @CompanyCity, @CompanyAddress, @CompanyZipCode, @CompanyProvince,
                            @Note, @ModifiedBy, @InsertedBy, @ModifiedDate, @InsertedDate);";

                        await coprelConnection.ExecuteAsync(query, importedCustomer);
                    }
                }
            }
        }
        catch (System.Exception)
        {

            throw;
        }

        async Task Log(string logMessage, bool importLog = false)
        {
            var today = DateTime.Now;
            var dateNow = today.ToString("dd_MM_yyyy");
            var dateTimeNow = today.ToString("dd/MM/yyyy HH:mm:ss");

            string fileName = importLog ? $"C:\\\\Temp\\\\Import_{dateNow}.txt" : $"C:\\\\Temp\\\\Log_{dateNow}.txt";

            if (!File.Exists(fileName))
            {
                var file = File.Create(fileName);
                file.Close();
            }

            var fileRow = $"{dateTimeNow} - {logMessage} \\r\\n";
            await File.AppendAllTextAsync(fileName, fileRow);

            Console.WriteLine(logMessage);
        }
    }
}