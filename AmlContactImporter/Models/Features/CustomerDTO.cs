namespace AmlContactImporter.Models.Features.DTO;

public class CustomerDTO
{
    public int Id { get; set; }

    public int? CustomerTypeId { get; set; }

    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string? CompanyName { get; set; }

    public string? VAT { get; set; }

    public string? FiscalCode { get; set; }

    public string? Email { get; set; }

    public string? PEC { get; set; }

    public string? SDI { get; set; }

    public string? PhoneNumber { get; set; }

    public string? Country { get; set; }

    public string? City { get; set; }

    public string? Address { get; set; }

    public string? ZipCode { get; set; }

    public string? Province { get; set; }

    public string? BirthCountry { get; set; }

    public string? BirthCity { get; set; }

    public string? BirthZipCode { get; set; }

    public string? BirthProvince { get; set; }

    public string? BirthDate { get; set; }

    public bool IsCompany { get; set; }

    public string? CompanyCountry { get; set; }

    public string? CompanyCity { get; set; }

    public string? CompanyAddress { get; set; }

    public string? CompanyZipCode { get; set; }

    public string? CompanyProvince { get; set; }

    public DateTime? AcquisitionDate { get; set; }

    public string? FolderName { get; set; }

    public string? Note { get; set; }

    public string? InsertedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? InsertedDate { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public override string ToString()
    {
        return $"{FirstName} - {LastName} - {CompanyName}";
    }
}