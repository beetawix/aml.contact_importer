namespace AmlContactImporter.Models.Features.DTO;

public class CustomerTypeDTO
{
    public int Id { get; set; }
    public string? Name { get; set; }
}